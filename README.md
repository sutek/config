# config

# Programs to install

* ranger
* pentadactyl
* calibre
* evolution
* htop
* inkscape
* mendeley
* nitrogen
* pcmanfm or thunar
* uzbl
* xfce4-terminal
* zathura
* dosbox
* eclipse
* rclone
* lxappearance
* pavucontrol
