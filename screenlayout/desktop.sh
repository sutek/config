#!/bin/sh
xrandr --output DP-4.8 --mode 1920x1200 --pos 1920x0 --rotate normal --output DVI-D-0 --off --output DP-2.8 --primary --mode 1920x1200 --pos 0x0 --rotate normal --output HDMI-0 --off --output DP-5 --off --output DP-4 --off --output DP-3 --off --output DP-2 --off --output DP-1 --off --output DP-0 --off
